from keras.models import load_model
import keras
import tensorflow as tf
import numpy as np
from time import time
import multiprocessing

def predict(image, model_file):
    tf.autograph.set_verbosity(0)
    config = tf.compat.v1.ConfigProto()
    config.gpu_options.allow_growth = True
    session = tf.compat.v1.Session(config=config)
    keras.backend.set_session(session)
    workers = multiprocessing.cpu_count()
    
    model = load_model(model_file)   
    _, w, h, _= model.input.shape
    pred_size = (h, w)
    resimg = image.resize(pred_size)
    resimg = np.array(resimg)
    resimg = resimg.astype(np.float32)
    resimg = resimg / 255
    resimg = np.expand_dims(resimg, axis=0)
   
    start = time()
    result = model.predict(resimg, verbose=1, workers=workers, use_multiprocessing=True)
    stop = time()
    pred_classes = np.argmax(result)
    return pred_classes, np.max(result), stop-start
