from keras.models import load_model
from keras.preprocessing import image
import keras
import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)
keras.backend.set_session(session)

model = load_model('RetiNet_D2.hdf5')
model1 = model.layers[-2]
model1.summary()
model1.save("test.hdf5")