import cv2
import numpy as np
import os
def get_output_layers(net):
    layer_names = net.getLayerNames()

    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers
# function to draw bounding box on the detected object with class name
def draw_bounding_box(image, class_id, confidence, x, y, x_plus_w, y_plus_h, class_names):
    img = image
    # read class names from text file
    with open(class_names, 'r') as f:
        classes = [line.strip() for line in f.readlines()]
    # generate different colors for different classes
    COLORS = [[0, 255, 255], [255, 127, 50]]
    label = str(classes[class_id])

    color = COLORS[class_id]

    cv2.rectangle(img, (x, y), (x_plus_w, y_plus_h), color, 5)

    cv2.putText(img, label+" "+str(round(confidence*100,2))+"%", (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX,
                2, color, 3)
    return img

# Function to detect objets single image
def detect(image,weights,config,class_names):
    Width = image.shape[0]
    Height = image.shape[1]
    scale = 0.00392
    class_names = class_names
    # read pre-trained model and config file
    net = cv2.dnn.readNet(weights, config)

    # create input blob
    blob = cv2.dnn.blobFromImage(image, scale, (416, 416), (0, 0, 0), True, crop=False)

    # set input blob for the network
    net.setInput(blob)

    # run inference through the network
    # and gather predictions from output layers
    outs = net.forward(get_output_layers(net))

    # initialization
    class_ids = []
    confidences = []
    boxes = []
    conf_threshold = 0.5
    nms_threshold = 0.4

    # for each detetion from each output layer
    # get the confidence, class id, bounding box params
    # and ignore weak detections (confidence < 0.5)
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * Width)
                center_y = int(detection[1] * Height)
                w = int(detection[2] * Width)
                h = int(detection[3] * Height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])
    # apply non-max suppression
    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
    # go through the detections remaining
    # after nms and draw bounding box
    macula_p = 0
    nervio_p = 0
    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        draw_bounding_box(image, class_ids[i], confidences[i], round(x), round(y), round(x + w), round(y + h),
                          class_names)
        if class_ids[i] == 0:
            macula_p = round(confidences[i], 4)
        elif class_ids[i] == 1:
            nervio_p = round(confidences[i], 4)

    #cv2.imwrite("detection.jpeg", image)
    return macula_p, nervio_p, image
