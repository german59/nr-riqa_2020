import cv2
import numpy as np
import math

def snr(rgb):
    gray = cv2.cvtColor(rgb,cv2.COLOR_BGR2GRAY) #conversion a escala de grises
    mean, std = cv2.meanStdDev(gray)
    snr=(mean/std)
    return float(snr)

def calcContrast(rgb):
    gray = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
    gray = np.asarray(gray)
    gray = gray / 255
    return float(gray.std())

def michelson(rgb):
    gray = cv2.cvtColor(rgb,cv2.COLOR_BGR2GRAY)
    hist,bins = np.histogram(gray.ravel(),256,[0,256])
    hist = np.trim_zeros(hist)
    mid = math.ceil(len(hist)/2)
    hminp = hist[:mid]
    hmaxp = hist[mid:]
    lmax = np.mean(hmaxp)
    lmin = np.mean(hminp)
    contrast = abs((lmax - lmin) / (lmax + lmin))
    return float(contrast)
def laplacian(rgb):
    blur = cv2.Laplacian(rgb,cv2.CV_64F).var()
    return float(blur)
