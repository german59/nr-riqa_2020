#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = ["Germán Pinedo"]
__copyright__ = "Copyright 2020"
__credits__ = ["Germán Pinedo"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Germán Pinedo"]
__email__ = "german.pinedo@cinvestav.mx"
__status__ = "Development"

import os
import argparse
from detector import *
from predict import *
from q_ind import *
from cropretina import crop_retina
import cv2
from PIL import Image
from time import time

ap = argparse.ArgumentParser()
ap.add_argument('-i', '--image', required=True,
                help = 'path to read image')
args = ap.parse_args()
model_name = 'models/crop_256.hdf5'
model = 'models/class.hdf5'
config = 'models/yolo_1.cfg'
weights = 'models/yolo_1.weights'
classes = 'models/nm.names'
start = time()
# Load image
img = Image.open(args.image).convert('RGB')
# Cropping process
crop = crop_retina(img, model_name)
image = np.array(crop)
# Convert RGB to BGR 
rgb = image[:, :, ::-1].copy()
# Quality params
SNR = snr(rgb)
CONT = calcContrast(rgb)
VAR_LAP = laplacian(rgb)

# YOLO detection
m_p, n_p, image = detect(rgb, weights, config, classes)
# Classification model
pred_class, prob_class, time_r = predict(crop, model)
stop = time()
#print(time_r)
print(stop-start)

font                   = cv2.FONT_HERSHEY_SIMPLEX
fontScale              = rgb.shape[0]/rgb.shape[1]+1
fontColor              = (0,200,0)
lineType               = 3
if (pred_class == 1 and m_p >= 0.5 and n_p >= 0.5 
	and SNR > 1 and (CONT < 0.27 and CONT > 0.12) 
	and (VAR_LAP < 64 and VAR_LAP > 10)):
    quality_def = 'Adequate'
else:
    quality_def = 'Inadequate'

cv2.putText(rgb,'Quality: '+ quality_def,
    (round(rgb.shape[0]/3),100),
    font,
    fontScale,
    (255,255,255),
    lineType)
cv2.putText(rgb,'Class: '+str(pred_class),
    (5,200),
    font,
    fontScale,
    fontColor,
    lineType)
cv2.putText(rgb,'Prob_Class: '+str(round(prob_class*100,4))+"%",
    (5,300),
    font,
    fontScale,
    fontColor,
    lineType)
cv2.putText(rgb,'SNR: '+str(round(SNR,2)),
    (5,400),
    font,
    fontScale,
    fontColor,
    lineType)
cv2.putText(rgb,'Contrast: '+str(round(CONT,2)),
    (5,500),
    font,
    fontScale,
    fontColor,
    lineType)
cv2.putText(rgb,'Blur: '+str(round(VAR_LAP,2)),
    (5,600),
    font,
    fontScale,
    fontColor,
    lineType)
file, ext = os.path.splitext(args.image)
filename = os.path.split(file)
cv2.imwrite(os.path.join(filename[1])+"_process"+ext,rgb)
